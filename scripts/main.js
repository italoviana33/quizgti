
var perguntas; //objeto json
let reque = new XMLHttpRequest();
var answers = document.getElementsByTagName('span');
var answersRadio=document.getElementsByName('answer');
var ContPerg = 0; //contador para pergunta atual
var Pts = 0; //pontuação

//Requesição do JSON
reque.open('GET','https://quiz-trainee.herokuapp.com/questions', true);
reque.send();
reque.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200)
     {
        perguntas = (JSON.parse(reque.responseText)); 
        console.log(perguntas.length);        
     }  
    }  
    
    function start() // Função que é chamada quando o butão é apertado
    {    
        
        if(ContPerg===perguntas.length-1)//verifica e altera o valor apresentado no butão
        {   
            document.getElementById('buton').innerHTML="CHECK";
        }else document.getElementById('buton').innerHTML="proxima";
        
        if(ContPerg===perguntas.length) //verifica se acabou as perguntas e chama a função que finaliza, reseta e apresenta o resultado
        {
            EndQuiz();
            return;
        }
        if (ContPerg>=0) //verifica se o quiz começou
        {
            if(ContPerg>=1)// verifica se há alguma opção selecionada
            {
               for (var i = 0; i < 4; i++)
               if (document.getElementsByName("answer")[i].checked) 
                    break;
                else if (i == 3) 
                    return;
            }
                       
            document.getElementById('answers').style.display='block'; //faz com que a lista apareça na tela          
            document.getElementById('Question').innerHTML=perguntas[ContPerg].title; //apresenta o titulo da questão   
            
            for(let b=0;b<perguntas[ContPerg].options.length;b++)// faz o preenchimento dos itens e valores da lista e soma os pontos da questão anterior
            {                
                if(answersRadio[b].checked){              
                    Pts += Number(answersRadio[b].value);
                }
                answersRadio[b].checked = false;
                answers[b].innerHTML=perguntas[ContPerg].options[b].answer;
                answersRadio[b].value=perguntas[ContPerg].options[b].value;
            }
            ContPerg++;       
        } 
    }
    function EndQuiz()// função que finaliza, reseta e apresenta o resultado
    {        
        for(let b=0;b<perguntas[ContPerg-1].options.length;b++)//contabiliza os pontos da ultima questão
        {
            if(answersRadio[b].checked){          
                Pts += Number(answersRadio[b].value);
            }
            answersRadio[b].checked = false;
        }        
        var porcentagem = (Pts/(3*perguntas.length)*100); //calculo para saber a porcentagem
        document.getElementById('Question').innerHTML='sua pontuação: '+ porcentagem +'%';//printa na tela o resultado
        if(porcentagem === 0)//caso alguem seja muito ruim, merece ser chamada de cuck
        {
            alert("PI PI PI");
            alert("ALERTA DE CUCK!!!");
        }
        document.getElementById('buton').innerHTML="Reset";//altera o valor do botão para reset
        document.getElementById('answers').style.display='none';//esconde a lista de itens
        Pts=0; // zera os pontos
        ContPerg=0; // reseta a contagem de perguntas
    }
